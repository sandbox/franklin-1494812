<?php

/* 
 * An array where the module reports what version DCDL it supports. 
 * The current and only version is 1.  All supporting modules should 
 * implement this.
 */
function HOOK_dcdl_api() {
  return array('version' => 1);
}

/*
 * SIMPLE MODULES
 * 
 * The vast majority of modues will only need one or both of the next
 * two functions.  The core DCDL has code to automatically create and
 * load the configuration based on the information provided by these
 * two hooks.
 */ 

/* The list of entries in the variables table managed by this module. 
 * DCDL will pull these variables from the variables table and save them
 * in the MODULE.conf.php file. 
 * 
 * Include: 
 *    Items set in administration forms
 * Do not include:
 *    State variables (e.g., timestamp of last e-mail batch run)
 *    Third-party service credientials (e.g., Acquia Network keys, Google Analytics Web Property IDs)
 */
function HOOK_config_variables() {
  return array('variable_key1', 'variable_key2');
}

/* 
 * The list of tables maintained by this module with configuration data. 
 * 
 * Include:
 *    Tables that are maintained in administration pages.
 * Do not include:
 *    Tables with cached or pre-processed data
 *    Tables that reference UIDs or NIDs or CIDs (comment IDs)
 *    Tables with data created by users
 *    Tables with live data (e.g., PayPal IPNs)
 */
function HOOK_config_tables() {
  return array('table_name1', 'table_name2');
}

/*
 * MORE COMPLEX MODULES
 * 
 * Some modules have to be more selective about what data is written out or
 * may have to do special processing of their configuration data.  In that 
 * case, one or more of the following hooks will be needed.
 */

/* 
 * A function to handle dumping the data.  Return a (nested) array.
 * This will be given to HOOK_dcdl_load().  
 *
 * If this is not defined, then the default version will call the two 
 * hooks in the SIMPLE MODULES section above and create a $config with 
 * the variables and content of the tables specified.
 * 
 * If you provide this hook, you almost certainly need HOOK_dcdl_load()
 * below as well.
 */
function HOOK_dcdl_dump(&$config) {
	
}

/* 
 * Called early in the load command becore any of the modules get their 
 * HOOK_dcdl_load() called. Sends no informaiton, expects no returns. 
 */
function HOOK_dcdl_pre_load() {
}

/* 
 * Given the $config array, configure the module.  The $config array 
 * is identical to the array returned by HOOK_dcdl_dump().  Using the
 * values in the
 */
function HOOK_dcdl_load($config=array()) {
}

/* 
 * Called immediately after the module MODULE has run its HOOK_dcdl_load().
 * Use this hook if you need to react to the configuration of another
 * module. 
 * 
 * For example, if you need to react to something Views does, create a function
 * mymodule_dcdl_load_after_views().
 */
function HOOK_dcdl_load_after_MODULE() {
}

/*
 * Peform any clean-up needed after all modules have configured themselves.
 * This is a good place to validate the data in your configuration tables and
 * check for orphaned or widowed references.
 */
function HOOK_dcdl_post_load() {
}


/**
 * SITE-SPECIFIC MANAGEMENT
 */
/*
 * Called after the module MODULE has built its config array,
 * but before the $config is written to disk.
 */
function HOOK_dcdl_config_alter_MODULE(&$config) {
  
}

/*
 * Called after the module $module has built its config array,
 * but before the $config is written to disk.
 */
function HOOK_dcdl_config_alter($module, &$config) {
  
}