<?php

function domain_nodetype_dcdl_api() {
  return array('version' => 1);
}

function domain_nodetype_config_variables() {
  $vars = array();

  foreach (array_keys(node_get_types()) as $type) {
    $vars[] = 'domain_nodetype_'. $type;
    $vars[] = 'domain_nodetype_default_domains_'. $type;
    $vars[] = 'domain_nodetype_default_domain_source_'. $type;
  }
  
  return $vars;
}