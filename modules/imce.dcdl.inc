<?php

function imce_dcdl_api() {
  return array('version' => 1);
}

function imce_config_variables() {
  return array(
    'imce_profiles',
    'imce_roles_profiles',
    'imce_settings_textarea',
    'imce_settings_replace',
    'imce_settings_thumb_method',
    'imce_settings_disable_private',
    'imce_custom_content',
    'imce_custom_process',
    'imce_custom_init',
    'imce_custom_scan',
    'imce_custom_response',
    'imce_settings_absurls',
  );
}