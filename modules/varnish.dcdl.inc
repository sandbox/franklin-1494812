<?php

function varnish_dcdl_api() {
  return array('version' => 1);
}

function varnish_config_variables() {
  // Does not save varnish_version, varnish_control_terminal or varnish_control_key.  These are per-installation settings.
  return array(
    'varnish_flush_cron',
    'varnish_socket_timeout',
    'varnish_cache_clear',
  );
}