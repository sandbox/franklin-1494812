<?php

function menu_block_dcdl_api() {
  return array('version' => 1);
}

function menu_block_config_variables() {
  $variables = array(
    'menu_block_ids',
    'menu_block_menu_order',
  );
  
  foreach (variable_get('menu_block_ids', array()) AS $delta) {
    $variables[] = "menu_block_{$delta}_title_link";
    $variables[] = "menu_block_{$delta}_admin_title";
    $variables[] = "menu_block_{$delta}_parent";
    $variables[] = "menu_block_{$delta}_level";
    $variables[] = "menu_block_{$delta}_follow";
    $variables[] = "menu_block_{$delta}_depth";
    $variables[] = "menu_block_{$delta}_expanded";
    $variables[] = "menu_block_{$delta}_sort";
  }
  
  return $variables;
}