<?php

function workflow_access_dcdl_api() {
  return array('version' => 1);
}

function workflow_access_config_tables() {
  return array(
    'workflow_access',
  );
}