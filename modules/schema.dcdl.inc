<?php

function schema_dcdl_api() {
  return array('version' => 1);
}

function schema_config_variables() {
  return array(
    'schema_status_report',
    'schema_suppress_type_warnings',
  );
}