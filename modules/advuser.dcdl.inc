<?php

function advuser_dcdl_api() {
  return array('version' => 1);
}

function advuser_config_variables() {
  return array(
    'advuser_new_notify',
    'advuser_new_subject',
    'advuser_new_mail',
    'advuser_modify_notify',
    'advuser_modify_subject',
    'advuser_modify_mail',
    'advuser_listno',
    'advuser_profile_fields',
  );
}