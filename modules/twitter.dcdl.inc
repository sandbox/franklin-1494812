<?php

function twitter_dcdl_api() {
  return array('version' => 1);
}

function twitter_config_variables() {
  return array(
    'twitter_consumer_key',
    'twitter_consumer_secret',
    'twitter_import',
    'twitter_expire',
    'twitter_global_name',
    'twitter_global_password',
    'twitter_types',
    'twitter_default_format',
    'twitter_api_url',
    'twitter_set_source',
  );
}