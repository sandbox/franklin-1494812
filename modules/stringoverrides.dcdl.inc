<?php

function stringoverrides_dcdl_api() {
  return array('version' => 1);
}

function stringoverrides_config_variables() {
  $variables = array();
  
  if (module_exists('i18n')) {
    foreach (array_keys(language_list()) as $langcode) {
      $variables[] = 'locale_custom_disabled_strings_' . $langcode;
      $variables[] = 'locale_custom_strings_' . $langcode;
    }
  }
  return $variables;
}