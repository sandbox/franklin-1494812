<?php

function advanced_help_dcdl_api() {
  return array('version' => 1);
}

/**
 * Create a trumped up dump designed to reset the database.
 * The module will automatically regenerate the index.
 */
function advanced_help_dcdl_dump(&$config) {
  $config['variables']['advanced_help_last_cron'] = 'DCDL_UNSET';
}
