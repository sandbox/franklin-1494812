<?php

function getid3_dcdl_api() {
  return array('version' => 1);
}

function getid3_config_variables() {
  return array(
    'getid3_path',
    'getid3_show_warnings',
  );
}