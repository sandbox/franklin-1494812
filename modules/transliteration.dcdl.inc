<?php

function transliteration_dcdl_api() {
  return array('version' => 1);
}

function transliteration_config_variables() {
  return array(
    'transliteration_file_uploads',
    'transliteration_file_lowercase',
  );
}