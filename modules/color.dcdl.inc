<?php

function color_dcdl_api() {
  return array('version' => 1);
}

function color_config_variables() {
  $vars = array();

  $themes = list_themes();
  foreach ($themes as $theme => $settings) {
    $vars[] = 'color_' . $theme . '_palette';
    $vars[] = 'color_' . $theme . '_stylesheets';
    $vars[] = 'color_' . $theme . '_logo';
    $vars[] = 'color_' . $theme . '_files';
    $vars[] = 'color_' . $theme . '_screenshot';
  }

  return $vars;
}