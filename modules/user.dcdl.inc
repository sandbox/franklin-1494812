<?php

function user_dcdl_api() {
  return array('version' => 1);
}

function user_config_variables() {
  return array(
    'anonymous',

    /* Registration */
    'user_register',
    'user_email_verification',
    'user_registration_help',

    /* Email notifications */
    'user_mail_register_admin_created_subject',
    'user_mail_register_admin_created_body',
    'user_mail_register_no_approval_required_subject',
    'user_mail_register_no_approval_required_body',
    'user_mail_register_pending_approval_subject',
    'user_mail_register_pending_approval_body',
    'user_mail_password_reset_subject',
    'user_mail_password_reset_body',
    'user_mail_status_activated_notify',
    'user_mail_status_activated_subject',
    'user_mail_status_activated_body',
    'user_mail_status_blocked_notify',
    'user_mail_status_blocked_subject',
    'user_mail_status_blocked_body',
    'user_mail_status_deleted_notify',
    'user_mail_status_deleted_subject',
    'user_mail_status_deleted_body',
    'user_signatures',

    /* Pictures */
    'user_pictures',
    'user_picture_path',
    'user_picture_default',
    'user_picture_dimensions',
    'user_picture_file_size',
    'user_picture_guidelines',

    /* User Block */
    'user_block_whois_new_count',
    'user_block_seconds_online',
    'user_block_max_list_count',
    'user_block_whois_new_count',
    
  );
}

function user_config_tables() {
  return array(
    'role',
    'permission', 
    'access',
  );
}