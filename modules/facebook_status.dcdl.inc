<?php

function facebook_status_dcdl_api() {
  return array('version' => 1);
}

function facebook_status_config_variables() {
  return array(
    'facebook_status_concat',
    'facebook_status_legacy',
    'facebook_status_twitter_default',
    'facebook_status_type',
    'facebook_status_filter',
    'facebook_status_exclude',
    'facebook_status_slide',
    'facebook_status_size',
    'facebook_status_length',
    'facebook_status_repost',
    'facebook_status_hide_status',
    'facebook_status_hide_blank',
    'facebook_status_nl2br',
    'facebook_status_ahah',
    'facebook_status_profile_view',
    'facebook_status_share_view',
    'facebook_status_reply_type',
    'facebook_status_size_long',
    'facebook_status_box_rows',
    'facebook_status_flood_user',
    'facebook_status_default_text',
  );
}