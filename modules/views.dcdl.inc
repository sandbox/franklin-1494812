<?php

function views_dcdl_api() {
  return array('version' => 1);
}

function views_config_variables() {
  return array(
    'views_block_hashes',
    'views_no_javascript',
    'views_defaults',
    'views_devel_output',
    'views_devel_region',
    'views_skip_cache',
    'views_sql_signature',
    'views_hide_help_message',
    'views_ui_query_on_top',
    'views_ui_disable_live_preview',
    'views_show_additional_queries',
    'views_no_hover_links',
    'views_exposed_filter_any_label',
  );
}

function views_config_tables() {
  return array(
    'views_view',
    'views_display' => array(
      'no uuid map' => array('id'),
    ),
  );
}