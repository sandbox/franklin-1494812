<?php

function fbconnect_dcdl_api() {
  return array('version' => 1);
}

function fbconnect_config_variables() {
  $variables = array(
    'fbconnect_appid',
    'fbconnect_skey',  
    'fbconnect_base_domaine',
    'fbconnect_connect_url',
    'fbconnect_uninstall_url',
    'fbconnect_language_code',
    'fbconnect_debug',
    'fbconnect_fast_reg',
    'fbconnect_reg_options',
    'fbconnect_loginout_mode',
    'fbconnect_invite_msg',
    'fbconnect_invite_name',  
    'fbconnect_invite_dest',
    'fbconnect_button',  
    'fbconnect_button_text',
    'fbconnect_pic_allow',
    'fbconnect_pic_size',
    'fbconnect_pic_logo', 
    'fbconnect_pic_size_nodes',
    'fbconnect_pic_size_comments',
    'fbconnect_pic_size_profile',
    
  );
  
  if (module_exists('i18n')) {
    foreach (array_keys(language_list()) as $langcode) {
      $variables[] = 'fbconnect_lanugage_code_' . $langcode;
    }
  }
  
  return $variables;
}