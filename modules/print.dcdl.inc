<?php

function print_dcdl_api() {
  return array('version' => 1);
}

function print_config_variables() {
  $vars = array(
    'print_settings',
    'print_sourceurl_settings',
    'print_html_settings',
    'print_robot_settings',
    'print_logo_url',
    'print_logo_options',
    'print_css',
    'print_keep_theme_css',
    'print_urls',
    'print_urls_anchors',
    'print_comments',
    'print_newwindow',
    'print_sourceurl_enabled',
    'print_sourceurl_date',
    'print_sourceurl_forcenode',
    'print_html_show_link',
    'print_html_link_pos',
    'print_html_link_teaser',
    'print_html_node_link_visibility',
    'print_html_node_link_pages',
    'print_html_link_class',
    'print_html_sys_link_visibility',
    'print_html_sys_link_pages',
    'print_html_book_link',
    'print_html_new_window',
    'print_html_sendtoprinter',
    'print_html_windowclose',
    'print_display_sys_urllist',
    'print_robots_noindex',
    'print_robots_nofollow',
    'print_robots_noarchive',
    'print_footer_options',
    'print_footer_user',
    'print_html_link_text',
    'print_html_link_use_alias',
    'print_text_links',
    'print_text_published',
    'print_text_retrieved',
    'print_text_source_url',
  );
  foreach(array_keys(node_get_types()) as $type) {
    $vars [] = 'print_display_' . $type;
    $vars [] = 'print_display_comment_' . $type;
    $vars [] = 'print_display_urllist_' . $type;
  }
  return $vars;
}

function print_mail_dcdl_api() {
  return array('version' => 1);
}

function print_mail_config_variables() {
  $vars = array(
    'print_mail_settings',
    'print_mail_show_link',
    'print_mail_link_pos',
    'print_mail_link_teaser',
    'print_mail_node_link_visibility',
    'print_mail_node_link_pages',
    'print_mail_link_class',
    'print_mail_sys_link_visibility',
    'print_mail_sys_link_pages',
    'print_mail_book_link',
    'print_mail_hourly_threshold',
    'print_mail_teaser_default',
    'print_mail_teaser_choice',
    'print_mail_link_text',
    'print_mail_link_use_alias',
    'print_mail_text_title',
    'print_mail_text_confirmation',
    'print_mail_text_message',
    'print_mail_text_subject',
    'print_mail_text_content',
    'print_mail_job_queue',
    'print_mail_display_sys_urllist',
  );
  foreach(array_keys(node_get_types()) as $type) {
    $vars [] = 'print_mail_display_' . $type;
    $vars [] = 'print_mail_display_comment_' . $type;
    $vars [] = 'print_mail_display_urllist_' . $type;
  }
  return $vars;
}