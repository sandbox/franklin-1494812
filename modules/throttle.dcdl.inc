<?php

function throttle_dcdl_api() {
  return array('version' => 1);
}

function throttle_config_variables() {
  return array(
    'throttle_anonymous',
    'throttle_user',
    'throttle_probablity_limiter',
  );
}