<?php

function taxonomy_image_dcdl_api() {
  return array('version' => 1);
}

function taxonomy_image_config_variables() {
  return array(
    'taxonomy_image_path',
    'taxonomy_image_disable',
    'taxonomy_image_link_title',
    'taxonomy_image_default',
    'taxonomy_image_imagecache_preset',
    'taxonomy_image_resize',
    'taxonomy_image_height',
    'taxonomy_image_width',
    'taxonomy_image_admin_preset',
    'taxonomy_image_wrapper',
    'taxonomy_image_recursive',
  );
}

function taxonomy_image_dcdl_dump(&$config) {
  /* Don't dump tags, only normal taxonomy */
  $filter = "tid in (SELECT td.tid FROM {term_data} td INNER JOIN {vocabulary} v ON td.vid = v.vid WHERE v.tags=0)";  
  $config['tables']['term_image'] = dcdl_dump_table('term_image', array('filter' => $filter));
}