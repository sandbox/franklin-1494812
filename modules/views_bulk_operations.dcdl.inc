<?php

function views_bulk_operations_dcdl_api() {
  return array('version' => 1);
}

function views_bulk_operations_config_variables() {
  return array(
    'views_bulk_operations_actions',
  );
}