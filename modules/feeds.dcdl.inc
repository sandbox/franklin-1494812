<?php

function feeds_dcdl_api() {
  return array('version' => 1);
}

function feeds_config_tables() {
  return array(
    'feeds_importer',
    'feeds_source',    
  );
}

function feeds_import_dcdl_api() {
  return array('version' => 1);
}
/* Import has no additional configuration data */

function feeds_news_dcdl_api() {
  return array('version' => 1);
}

/* News has no additional configuration data */

function feeds_ui_dcdl_api() {
  return array('version' => 1);
}

function feeds_ui_config_variables() {
  return array(
    'default_feeds_importer',
  );
}