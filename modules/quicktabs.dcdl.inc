<?php

function quicktabs_dcdl_api() {
  return array('version' => 1);
}

function quicktabs_config_variables() {
  return array(
    'quicktabs_tabstyle',
  );
}

function quicktabs_config_tables() {
  return array(
    'quicktabs',
  );
}