<?php

function poormanscron_dcdl_api() {
  return array('version' => 1);
}

function poormanscron_config_variables() {
  return array(
    'poormanscron_interval',
    'poormanscron_retry_interval',
    'poormanscron_log_cron_runs',
    'cron_safe_threshold',
    'cron_threshold_semaphore', 
  );
}