<?php

function author_pane_dcdl_api() {
  return array('version' => 1);
}

function author_pane_config_variables() {
  return array(
    'author_pane_block_display_types',
    'author_pane_block_user_picture_preset',
  );
}