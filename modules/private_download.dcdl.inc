<?php

function private_download_dcdl_api() {
  return array('version' => 1);
}

function private_download_config_variables() {
  return array(
    'private_download_directory',
    'private_download_htaccess',
    'private_download_header',
  );
}