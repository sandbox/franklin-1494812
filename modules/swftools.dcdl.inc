<?php

function swftools_dcdl_api() {
  return array('version' => 1);
}

function swftools_config_variables() {
  return array(
    SWFTOOLS_EMBED_METHOD,
    'swftools_always_add_js',
    'swftools_user_no_js',
    'swftools_html_alt',
    'swftools_params_version',
    'swftools_params_play',
    'swftools_params_loop',
    'swftools_params_menu',
    'swftools_params_allowfullscreen',
    'swftools_params_bgcolor',
    'swftools_params_quality',
    'swftools_params_scale',
    'swftools_params_wmode',
    'swftools_params_align',
    'swftools_params_salign',
    'swftools_params_swliveconnect',
    'swftools_params_allowscriptaccess',
    'swftools_player_path',
    'swftools_playlist_path',
    'swftools_media_url',
    'swftools_media_url_dummy',
    'swftools_check_media',
    'swftools_playlist_caching',
    'swftools_grant_access_to_private_files',
    'swftools_grant_access_extensions',
  );
}