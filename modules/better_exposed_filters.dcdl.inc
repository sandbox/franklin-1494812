<?php

function better_exposed_filters_dcdl_api() {
  return array('version' => 1);
}

/* This module saves no configuration data outside of the views.  Any data is captured by the Views module. */