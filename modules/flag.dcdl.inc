<?php

function flag_dcdl_api() {
  return array('version' => 1);
}

function flag_config_variables() {
  return array(
  );
}

function flag_config_tables() {
  return array(
    'flags',
    'flag_types'
  );
}