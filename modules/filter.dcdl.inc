<?php

function filter_dcdl_api() {
  return array('version' => 1);
}

function filter_config_variables() {
  $vars = array(
    'filter_default_format',
    'filter_allowed_protocols'
  );
  
  $formats = array();
  $result = db_query('SELECT format FROM {filter_formats}');
  while ($record = db_fetch_array($result)) {
   $formats[] = $record['format'];
  }
  
  foreach ($formats as $format) {
    $vars[] = 'filter_html_' . $format;
    $vars[] = 'filter_html_help_' . $format;
    $vars[] = 'allowed_html_' . $format;
    $vars[] = 'filter_html_nofollow_' . $format;

    $vars[] = 'filter_url_length_' . $format;
  }
  
  return $vars;
}

function filter_config_tables() {
  return array('filter_formats', 'filters');
}