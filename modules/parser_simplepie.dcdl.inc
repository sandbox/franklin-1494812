<?php

function parser_simplepie_dcdl_api() {
  return array('version' => 1);
}

function parser_simplepie_config_variables() {
  return array(
    'cache_lifetime',
    'parser_simplepie_cache',
  );
}