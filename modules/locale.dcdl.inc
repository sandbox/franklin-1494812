<?php

function locale_dcdl_api() {
  return array('version' => 1);
}

function locale_config_variables() {
  $vars = array(
    'language_default',
    'language_count',
    'language_content_type_default',
    'language_content_type_negotiation',
    'locale_cache_strings',
    'locale_js_directory',
    'javascript_parsed',
    'language_negotiation',
  );
  
  foreach (node_get_types() as $type => $content_type) {
    $vars[] = "language_content_type_$type";
  }
  
  return $vars;
}

function locale_config_tables() {
  return array('languages');
}