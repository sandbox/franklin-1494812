<?php

function node_dcdl_api() {
  return array('version' => 1);
}

function node_config_variables() {
  $variables = array(
    'default_nodes_main',
    'teaser_length',
    'node_preview',
  );
  
  foreach (array_keys(node_get_types()) as $type) {
    $variables[] =  'node_options_' . $type;
  }
  
  return $variables ;
}

function node_config_tables() {
  return array(
    'node_type' => array(
      'no uuid map' => array('type'),
    ),
  );
}