<?php

function mollom_dcdl_api() {
  return array('version' => 1);
}

function mollom_config_variables() {
  return array(
    'mollom_fallback',
    'mollom_privacy_link',
    'mollom_testing_mode',
  );
}