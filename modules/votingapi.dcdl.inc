<?php

function votingapi_dcdl_api() {
  return array('version' => 1);
}

function votingapi_config_variables() {
  return array(
    'votingapi_anonymous_window',
    'votingapi_calculation_schedule',
  );
}