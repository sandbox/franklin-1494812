<?php

function date_timezone_dcdl_api() {
  return array('version' => 1);
}

function date_timezone_config_variables() {
  return array(
    'date_default_timezone_name',
  );
}