<?php

function contact_dcdl_api() {
  return array('version' => 1);
}

function contact_config_variables() {
  return array(
    'contact_default_status',
    'contact_form_information',
    'contact_hourly_threshold',
  );
}

function contact_config_tables() {
  return array('contact');
}