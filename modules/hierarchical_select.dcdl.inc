<?php

function hierarchical_select_dcdl_api() {
  return array('version' => 1);
}

function hierarchical_select_config_variables() {
  $vars = array();
  
  foreach (taxonomy_get_vocabularies() as $vid => $vocabulary) {
    $vars[] = 'hierarchical_select_save_lineage_' . $vid;
    $vars[] = 'hierarchical_select_enforce_deepest_' . $vid;
    $vars[] = 'hierarchical_select_multiple_' . $vid;
    $vars[] = 'hierarchical_select_dropbox_title_' . $vid;
    $vars[] = 'hierarchical_select_dropbox_limit_' . $vid;
  }
  
  return $vars;
}

function hs_taxonomy_dcdl_api() {
  return array('version' => 1);
}

function hs_taxonomy_config_variables() {
  $vars = array(
    'taxonomy_override_selector',
  );
  
  foreach (taxonomy_get_vocabularies() as $vid => $vocabulary) {
    $vars[] = 'taxonomy_hierarchical_select_' . $vid;
  }
  
  return $vars;
}