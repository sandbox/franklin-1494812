<?php

function comment_notify_dcdl_api() {
  return array('version' => 1);
}

function comment_notify_config_variables() {
  return array(
    'node_notify_default_mailtext',
    'comment_notify_available_alerts',
    'comment_notify_node_types',
    'comment_notify_default_anon_mailalert',
    'comment_notify_default_registered_mailalert',
    'node_notify_default_mailalert',
    'comment_notify_default_mailtext',
  );
}