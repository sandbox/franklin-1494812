<?php

function panels_dcdl_api() {
  return array('version' => 1);
}

function panels_config_variables() {
  
}

function panels_config_tables() {
  return array(
    'panels_renderer_pipeline',
    'panels_layout',
    'panels_display',
    'panels_pane',
  );
}