<?php

function dhtml_menu_dcdl_api() {
  return array('version' => 1);
}

function dhtml_menu_config_variables() {
  return array(
    'dhtml_menu_effects',
    'dhtml_menu_disabled',
  );
}