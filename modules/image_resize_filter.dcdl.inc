<?php

function image_resize_filter_dcdl_api() {
  return array('version' => 1);
}

function image_resize_filter_config_variables() {
  $vars = array();
  
  foreach (array_keys(filter_formats()) as $format) {
    $vars[] = 'image_resize_filter_link_' . $format;
    $vars[] = 'image_resize_filter_link_class_' . $format;
    $vars[] = 'image_resize_filter_link_rel_' . $format;
    $vars[] = 'image_resize_filter_image_locations_' . $format;
  }
  
  return $vars;
}