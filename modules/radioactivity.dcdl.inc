<?php

function radioactivity_dcdl_api() {
  return array('version' => 1);
}

function radioactivity_config_variables() {
  return array(
    'radioactivity_profiles',
    'radioactivity_memcached_enable',
    'radioactivity_memcached_expiration',
    'radioactivity_decay_granularity',
  );
}

function radioactivity_fivestar_voting_dcdl_api() {
  return array('version' => 1);
}

function radioactivity_node_dcdl_api() {
  return array('version' => 1);
}

function radioactivity_node_config_variables() {
  return array(
    'radioactivity_node_click_duration',
    'radioactivity_node_anon_hook_mode',
  );
}