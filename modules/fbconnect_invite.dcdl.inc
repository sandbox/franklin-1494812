<?php

function fbconnect_invite_dcdl_api() {
  return array('version' => 1);
}

function fbconnect_invite_config_variables() {
  return array(
    'fbconnect_invite_msg',
    'fbconnect_invite_label',
    'fbconnect_invite_name',
  );
}