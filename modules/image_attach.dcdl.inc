<?php

function image_attach_dcdl_api() {
  return array('version' => 1);
}

function image_attach_config_variables() {
  return array(
    'image_attach_existing',
    'image_attach_block_0_size',
  );
}