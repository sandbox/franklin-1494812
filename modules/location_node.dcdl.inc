<?php

function location_node_dcdl_api() {
  return array('version' => 1);
}

function location_node_config_variables() {
  $variables = array();
  
  foreach(array_keys(node_get_types()) as $node_type) {
    $variables[] = 'location_settings_node_'. $node_type;    
  }
  
  return $variables;
}