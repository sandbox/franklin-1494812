<?php

function skinr_dcdl_api() {
  return array('version' => 1);
}

function skinr_config_variables() {
  $vars = array();
  
  $themes = list_themes();
  foreach(array_keys($themes) as $theme_name) {
    $vars[] = 'skinr_' . $theme_name;
  }

  foreach (array_keys(node_get_types()) as $type) {
    $vars[] = 'skinr_settings_'. $type;
  }

  return $vars;
}