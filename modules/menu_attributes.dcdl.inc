<?php

function menu_attributes_dcdl_api() {
  return array('version' => 1);
}

function menu_attributes_config_variables() {
  $vars = array();
  $attributes = menu_attributes_menu_attribute_info();
  foreach (array_keys($attributes) as $attribute) {
    $vars[] = "menu_attributes_{$attribute}_enable";
    $vars[] = "menu_attributes_{$attribute}_default";
  }

  return $vars;
}