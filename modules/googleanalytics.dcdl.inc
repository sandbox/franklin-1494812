<?php

function googleanalytics_dcdl_api() {
  return array('version' => 1);
}

/* The googleanalytics_account is intentionally left out from the
   saved configuration.  Installing the production account ID on
   a dev or testing server will pollute the analytics data, and
   installing a dev or testing account ID in production will lose
   production data.  Best to leave it out.  */

function googleanalytics_config_variables() {
  return array(
    'googleanalytics_domain_mode',
    'googleanalytics_visibility',
    'googleanalytics_pages',
    'googleanalytics_visibility_roles',
    'googleanalytics_roles',
    'googleanalytics_custom',
    'googleanalytics_trackoutgoing',
    'googleanalytics_trackmailto',
    'googleanalytics_trackfiles',
    'googleanalytics_trackfiles_extensions',
    'googleanalytics_trackoutboundaspageview',
    'googleanalytics_tracker_anonymizeip',
    'googleanalytics_privacy_donottrack',
    'googleanalytics_segmentation',
    'googleanalytics_segmentation_DEPRECATED',
    'googleanalytics_segmentation_default_fields',
    'googleanalytics_custom_var',
    'googleanalytics_cache',
    'googleanalytics_translation_set',
    'googleanalytics_site_search',
    'googleanalytics_trackadsense',
    'googleanalytics_codesnippet_before',
    'googleanalytics_codesnippet_after',
    'googleanalytics_js_scope',
  );
}