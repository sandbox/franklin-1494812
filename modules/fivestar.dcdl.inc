<?php

function fivestar_dcdl_api() {
  return array('version' => 1);
}

function fivestar_config_variables() {
  $vars = array(
    'fivestar_widget',
    'fivestar_color_widget',
  );
  
  foreach(node_get_types('names') as $type) {
    $vars[] = 'fivestar_' . $type;
    $vars[] = 'fivestar_stars_' . $type;
    $vars[] = 'fivestar_position_teaser_' . $type . '_above';
    $vars[] = 'fivestar_position_' . $type . '_above';
    $vars[] = 'fivestar_style_' . $type . '_average';
    $vars[] = 'fivestar_text_' . $type . '_dual';
    $vars[] = 'fivestar_title_' . $type;
    $vars[] = 'fivestar_unvote_' . $type;
    $vars[] = 'fivestar_feedback_' . $type;
    $vars[] = 'fivestar_labels_enable_' . $type;
    $vars[] = 'fivestar_labels_' . $type;
  }
  $vars[] = 'fivestar_stars_default';
  
  return $vars;
}

function fivestar_comment_dcdl_api() {
  return array('version' => 1);
}

function fivestar_comment_config_variables() {
  return array(
  );
}
