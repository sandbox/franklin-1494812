<?php

function signup_dcdl_api() {
  return array('version' => 1);
}

function signup_config_variables() {
  return array(
  'signup_close_earl  y',
    'signup_node_settings_form',
    'signup_date_format',
    'signup_ignore_default_fields',
    'signup_form_location',
    'signup_user_reg_form',
    'signup_fieldset_collapsed',
    'signup_display_signup_admin_list',
    'signup_display_signup_user_list',
    'signup_user_list_view',
    'signup_admin_list_view',
  );
}