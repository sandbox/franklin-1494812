<?php

function forum_dcdl_api() {
  return array('version' => 1);
}

function forum_config_variables() {
  return array(
    'forum_containers',
    'forum_nav_vocabulary',
    'forum_order',
    'forum_per_page',
    'forum_hot_topic',
  );
}