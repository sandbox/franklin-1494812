<?php

function date_api_dcdl_api() {
  return array('version' => 1);
}

function date_api_config_variables() {
  return array(
    'date_max_year',
    'date_min_year',
    'date_php_min_year',
    'date_api_version',
    'date_default_timezone_name',
    'date_db_tz_support',
    'date_api_use_iso8601',
  );
}

function date_api_config_tables() {
  return array(
    'date_format_types' => array(
      'no uuid map' => array('type'),
    ),
    'date_formats',
    'date_format_locale',   
  );
}