<?php

function profile_dcdl_api() {
  return array('version' => 1);
}

function profile_config_variables() {
  return array(
    'profile_block_author_fields',
  );
}

function profile_config_tables() {
  return array('profile_fields');
}