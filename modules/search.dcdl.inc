<?php

function search_dcdl_api() {
  return array('version' => 1);
}

function search_config_variables() {
  return array(
    'search_cron_limit',
    'minimum_word_size',
    'overlap_cjk',
  );
}