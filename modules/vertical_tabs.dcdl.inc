<?php

function vertical_tabs_dcdl_api() {
  return array('version' => 1);
}

function vertical_tabs_config_variables() {
  return array(
    'vertical_tabs_forms',
    'vertical_tabs_default',
    'vertical_tabs_minimum',
    'vertical_tabs_node_type_settings',
  );
}