<?php

function feedapi_inherit_dcdl_api() {
  return array('version' => 1);
}

function feedapi_inherit_config_variables() {
  return array(
    'inherit_og',
    'inherit_language',
    'inherit_taxonomy',
    'inherit_author',
  );
}