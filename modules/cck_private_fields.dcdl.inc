<?php

function cck_private_fields_dcdl_api() {
  return array('version' => 1);
}

function cck_private_fields_config_variables() {
  $vars = array(
    'cck_private_fields_view_access_module',
  );
  
  $fields = content_fields();
  foreach (array_keys($fields) as $fieldname) {
    $vars[] = 'cck_private_fields_' . $fieldname;
  }
  
  return $vars;
}