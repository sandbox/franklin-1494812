<?php

function eloqua_dcdl_api() {
  return array('version' => 1);
}

function eloqua_config_variables() {
  return array(
    ELOQUA_VARIABLE_SITE_ID,
    ELOQUA_VARIABLE_SCRIPTS_DIRECTORY,
    ELOQUA_SETTINGS_BATCH_SIZE,
  );
}