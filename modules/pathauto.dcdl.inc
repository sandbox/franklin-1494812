<?php

function pathauto_dcdl_api() {
  return array('version' => 1);
}

function pathauto_config_variables() {
  return array(
    'pathauto_modulelist',
    'pathauto_taxonomy_supportsfeeds', 
    'pathauto_taxonomy_pattern', 
    'pathauto_taxonomy_bulkupdate',
    'pathauto_taxonomy_applytofeeds',
    'pathauto_taxonomy_2_pattern',
    'pathauto_taxonomy_1_pattern',
    'pathauto_modulelist',
    'pathauto_ignore_words',
    'pathauto_indexaliases',
    'pathauto_indexaliases_bulkupdate',
    'pathauto_max_component_length',
    'pathauto_max_length',
    'pathauto_node_bulkupdate',
    'pathauto_node_forum_pattern',
    'pathauto_node_image_pattern',
    'pathauto_node_page_pattern',
    'pathauto_node_pattern',
    'pathauto_node_story_pattern',
    'pathauto_punctuation_quotes',
    'pathauto_separator',
    'pathauto_update_action',
    'pathauto_user_bulkupdate',
    'pathauto_user_pattern',
    'pathauto_user_supportsfeeds',
    'pathauto_verbose',
    'pathauto_node_applytofeeds',
    'pathauto_punctuation_hyphen',
  );
}