<?php

function apachesolr_dcdl_api() {
  return array('version' => 1);
}

function apachesolr_config_variables() {
  /* Don't include apachesolr_index_last, that's state data. */
  return array(
    'apachesolr_rows',
    'apachesolr_facet_show_children',
    'apachesolr_facet_query_limits',
    'apachesolr_facet_query_initial_limits',
    'apachesolr_facet_query_limit_default',
    'apachesolr_facet_missing',
    'apachesolr_site_hash',
    'apachesolr_mlt_blocks',
    'apachesolr_cron_limit',
    'apachesolr_enabled_facets',
    'apachesolr_exclude_nodeapi_types',
  );
}