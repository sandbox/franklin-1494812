<?php

function domain_nav_dcdl_api() {
  return array('version' => 1);
}

function domain_nav_config_variables() {
  return array(
    'domain_nav_block',
    'domain_nav_theme',
  );
}