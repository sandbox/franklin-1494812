<?php

function webform_dcdl_api() {
  return array('version' => 1);
}

function webform_config_variables() {
  $variables = array(
    'webform_node_types',
    'webform_node_types_primary',
    'webform_use_cookies',
    'webform_default_from_address',
    'webform_default_from_name',
    'webform_default_subject',
    'webform_default_format',
    'webform_format_override',
    'webform_csv_delimiter',
    'webform_allowed_tags',
    'webform_blocks',
  );
  
  $path = drupal_get_path('module', 'webform') . '/components';
  $files = file_scan_directory($path, '^.*\.inc$');
  foreach ($files as $filename => $file) {
    $variables[] = 'webform_enable_' . $file->name;
  }
  
  return $variables;
}

/*
  We shouldn't save nodes as configuration data,
  but many webforms *are* more site config than
  site content. TODO: Revisit.
*/
/*
function webform_config_tables() {
  return array(
    'webform',
    'webform_component',
    'webform_emails',
    'webform_roles',
  );
}
*/