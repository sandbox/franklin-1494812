<?php

function apachesolr_search_dcdl_api() {
  return array('version' => 1);
}

function apachesolr_search_config_variables() {
  return array(
    'apachesolr_search_date_boost',
    'apachesolr_search_comment_boost',
    'apachesolr_search_changed_boost',
    'apachesolr_search_sticky_boost',
    'apachesolr_search_promote_boost',
    'apachesolr_search_query_fields',
    'apachesolr_search_type_boosts',
    'apachesolr_search_excluded_types',
  );
}