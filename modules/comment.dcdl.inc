<?php

function comment_dcdl_api() {
  return array('version' => 1);
}

function comment_config_variables() {
  $vars = array();
  
  $settings = array(
    'comment_default_mode',
    'comment_default_order',
    'comment_default_per_page',
    'comment_controls',
    'comment_anonymous',
    'comment_subject_field',
    'comment_preview',
    'comment_form_location',
  );
  
  $types = node_get_types();
  foreach (array_keys($types) as $type) {
    foreach ($settings as $setting) {
      $vars[] = $setting . '_' . $type;
    }
  }
  
  return $vars;
}