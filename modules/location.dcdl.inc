<?php

function location_dcdl_api() {
  return array('version' => 1);
}

function location_config_variables() {
  $variables = array(
    'location_default_country',
    'location_display_location',
    'location_use_province_abbreviation',
    'location_usegmap',
    'location_locpick_macro',
    'location_jit_geocoding',
    'location_maplink_external',
    'location_maplink_external_method',
    'location_geocode_google_minimum_accuracy',
  );
  
  foreach (array_keys(_location_supported_countries()) as $country_iso) {
    $variables[] = 'location_map_link_'. $country_iso;
    $variables[] = 'location_geocode_'. $country_iso;
  }
  
  return $variables;
}