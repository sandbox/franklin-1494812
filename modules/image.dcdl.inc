<?php

function image_dcdl_api() {
  return array('version' => 1);
}

function image_config_variables() {
  return array(
    'image_max_upload_size',
    'image_updated',
    'image_default_path',
    'image_sizes',
    'image_block_0_number_images',
    'image_block_1_number_images',
  );
}