<?php

function pngbehave_dcdl_api() {
  return array('version' => 1);
}

function pngbehave_config_variables() {
  return array(
    'pngbehave_css_classes',
    'pngbehave_css_exclude',
  );
}