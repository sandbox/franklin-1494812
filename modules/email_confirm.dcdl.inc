<?php

function email_confirm_dcdl_api() {
  return array('version' => 1);
}

function email_confirm_config_variables() {
  return array(
    'email_confirm_confirmation_email_subject',
    'email_confirm_confirmation_email_author',
    'email_confirm_confirmation_email_bcc',
    'email_confirm_confirmation_email_body',
    'email_confirm_confirmation_original_email_body',
  );
}