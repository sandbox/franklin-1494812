<?php

function domain_theme_dcdl_api() {
  return array('version' => 1);
}

function domain_theme_config_variables() {
  return array(
  );
}

function domain_theme_config_tables() {
  return array(
    'domain_theme' => array(
      'no uuid map' => array('theme'),
    ),
  );
}