<?php

function addthis_dcdl_api() {
  return array('version' => 1);
}

function addthis_config_variables() {
  return array(
    'addthis_username',
    'addthis_display_in_links',
    'addthis_display_in_teasers',
    'addthis_image',
    'addthis_image_secure',
    'addthis_image_width',
    'addthis_image_height',
    'addthis_image_attributes_alt',
    'addthis_image_attributes_class',
    'addthis_dropdown_disabled',
    'addthis_logo',
    'addthis_logo_background',
    'addthis_logo_color',
    'addthis_brand',
    'adthis_options',
    'addthis_offset_top',
    'addthis_offset_left',
    'addthis_disable_flash',
    'addthis_widget_version',
  );
}