<?php

function date_popup_dcdl_api() {
  return array('version' => 1);
}

function date_popup_config_variables() {
  return array(
    'date_popup_css_file',
    'date_popup_timepicker',
  );
}