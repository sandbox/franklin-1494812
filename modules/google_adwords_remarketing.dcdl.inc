<?php

function google_adwords_remarketing_dcdl_api() {
  return array('version' => 1);
}

function google_adwords_remarketing_config_tables() {
  return array(
    'google_adwords_remarketing',
    'google_adwords_remarketing_roles',
  );
}