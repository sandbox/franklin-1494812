<?php

function adminrole_dcdl_api() {
  return array('version' => 1);
}

function adminrole_config_variables() {
  return array(
    'adminrole_adminrole',
    'user_admin_role',
    'adminrole_exclude_permissions',
  );
}
