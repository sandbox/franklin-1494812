<?php

function advanced_forum_dcdl_api() {
  return array('version' => 1);
}

function advanced_forum_config_variables() {
  return array(
    'advanced_forum_style',
    'advanced_forum_image_directory',
    'advanced_forum_button_links',
    'advanced_forum_theme_all_comments',
    'advanced_forum_hide_created',
    'advanced_forum_topic_pager_max',
    'advanced_forum_topic_title_length',
    'advanced_forum_time_ago_cutoff',
    'advanced_forum_get_new_comments',
    'advanced_forum_use_topic_navigation',
    'advanced_forum_user_picture_preset',
  );
}