<?php

function ed_readmore_dcdl_api() {
  return array('version' => 1);
}

function ed_readmore_config_variables() {
  return array(
    'ed_readmore_remove',
    'ed_readmore_placement',
    'ed_readmore_text',
    'ed_readmore_title',
    'ed_readmore_tokens',
    'ed_readmore_nofollow',
    'ed_readmore_separator',
  );
}