<?php

function filefield_dcdl_api() {
  return array('version' => 1);
}

function filefield_config_variables() {
  return array(
    'filefield_inline_types',
    'filefield_description_type',
    'filefield_description_length',
    'filefield_icon_theme',
  );
}

function filefield_meta_dcdl_api() {
  return array('version' => 1);
}

function filefield_meta_config_variables() {
  return array(
    'filefield_meta_tags'
  );
}
