<?php

function domain_conf_dcdl_api() {
  return array('version' => 1);
}

function domain_conf_dcdl_dump(&$config) {
  // Only save config variables that other modules say should be saved.
  $all_variables = module_invoke_all('config_variables');
  if (!empty($all_variables)) {
    $where = "name IN ('" . implode("','", $all_variables) . "')";    
    $config['tables']['domain_conf'] = dcdl_dump_table('domain_conf', array('filter' => $where, 'no uuid map' => array('name')));
  }
}