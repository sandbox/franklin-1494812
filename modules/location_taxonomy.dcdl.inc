<?php

function location_taxonomy_dcdl_api() {
  return array('version' => 1);
}

function location_taxonomy_config_variables() {
  $variables = array();
  
  foreach(array_keys(taxonomy_get_vocabularies()) as $node_type) {
    $variables[] = 'location_taxonomy_'. $node_type;    
  }
  
  return $variables;
}