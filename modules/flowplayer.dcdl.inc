<?php

function flowplayer_dcdl_api() {
  return array('version' => 1);
}

function flowplayer_config_variables() {
  // The selector controls
  $names = array(
    'backgroundColor' => t('Control bar'),
    'sliderColor' => t('Sliders'),
    'buttonColor' => t('Buttons'),
    'buttonOverColor' => t('Mouseover'),
    'durationColor' => t('Total time'),
    'timeColor' => t('Time'),
    'progressColor' => t('Progress'),
    'bufferColor' => t('Buffer'),
  );
  
  $vars = array(
    'flowplayer_scaling',
    'flowplayer_buttons',
    'flowplayer_background_gradient',
    'flowplayer_border_radius',
  );
  
  foreach($names as $name => $clean_name) {
    $vars[] = 'flowplayer_color_' . $name;
  }
}