<?php

function scheduler_dcdl_api() {
  return array('version' => 1);
}

function scheduler_config_variables() {
  $vars = array(
    'scheduler_date_format',
    'scheduler_field_type',
    'scheduler_extra_info',
  );
  
  $node_variables = array(
    'scheduler_publish_enable',
    'scheduler_publish_touch',
    'scheduler_publish_required',
    'scheduler_publish_revision',
    'scheduler_unpublish_enable',
    'scheduler_unpublish_required',
    'scheduler_unpublish_revision',
  );
  
  foreach (array_keys(node_get_types('names')) as $type) {
    foreach($node_variables as $v) {
      $vars[] = $v . '_' . $type;
    }
  }
  
  return $vars;
}