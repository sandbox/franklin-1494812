<?php

function blogapi_dcdl_api() {
  return array('version' => 1);
}

function blogapi_config_variables() {
  $vars = array(
    'blogapi_node_types',
    'blogapi_extensions_default',
    'blogapi_uploadsize_default',
    'blogapi_usersize_default',
  );
  
  foreach (user_roles() as $rid => $role) {
    $vars[] = 'blogapi_extensions_' . $rid;
    $vars[] = 'blogapi_uploadsize_' . $rid;
    $vars[] = 'blogapi_usersize_' . $rid;
  }
  
  return $vars; 
}