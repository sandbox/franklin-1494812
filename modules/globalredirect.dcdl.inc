<?php

function globalredirect_dcdl_api() {
  return array('version' => 1);
}

function globalredirect_config_variables() {
  return array(
    'globalredirect_deslash',
    'globalredirect_nonclean2clean',
    'globalredirect_trailingzero',
    'globalredirect_menu_check',
    'globalredirect_case_sensitive_urls',
  );
}