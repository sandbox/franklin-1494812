<?php

function img_assist_dcdl_api() {
  return array('version' => 1);
}

function img_assist_config_variables() {
  return array(
    'img_assist_page_styling',
    'img_assist_link',
    'img_assist_all',
    'img_assist_pages',
    'img_assist_link',
    'img_assist_preview_count',
    'img_assist_max_size',
    'img_assist_popup_label',
    'img_assist_default_label',
    'img_assist_create_derivatives',
    'img_assist_default_link_behavior',
    'img_assist_default_link_url',
    'img_assist_default_insert_mode',
    'img_assist_load_title',
    'img_assist_load_description',
    'img_assist_vocabs',
    'img_assist_default_alignment',
    'img_assist_all',
  );
}