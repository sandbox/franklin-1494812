<?php

function location_search_config_variables() {
  return array(
    'location_search_map',
    'location_search_map_address',
    'location_search_map_macro',
  );
}