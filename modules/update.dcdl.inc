<?php
function update_dcdl_api() {
  return array('version' => 1);
}

function update_config_variables() {
  return array(
    'update_notify_emails',
    'update_check_frequency',
    'update_notification_threshold',
  );
}