<?php

function content_profile_dcdl_api() {
  return array('version' => 1);
}

/* Created content types are handled by the node module */

function content_profile_config_variables() {
  $variables = array(
    'content_profile_profile',
  );
  
  foreach (node_get_types('names') as $typename => $visiblename) {
    $variables[] = 'content_profile_use_' . $typename;
    $variables[] = 'content_profile_' . $typename;
  }
  
  return $variables;
}