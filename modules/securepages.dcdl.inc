<?php

function securepages_dcdl_api() {
  return array('version' => 1);
}

function securepages_config_variables() {
  return array(
    'securepages_enable',
    'securepages_switch',
    'securepages_secure',
    'securepages_pages',
    'securepages_ignore',
    'securepages_basepath',
    'securepages_basepath_ssl',  
  );
}