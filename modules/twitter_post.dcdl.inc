<?php

function twitter_post_dcdl_api() {
  return array('version' => 1);
}

function twitter_post_config_variables() {
  $variables = array(
    'twitter_post_types',
    'twitter_post_default_format',
    'twitter_post_default_state',
  );
  
  foreach (array_keys(node_get_types()) as $type) {
    $variables[] = 'twitter_post_default_format_' . $type;
    $variables[] = 'twitter_post_default_state_' . $type;
  }
  
  return $variables;
}