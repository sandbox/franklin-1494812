<?php

function already_in_dcdl_api() {
  return array('version' => 1);
}

function already_in_config_variables() {
  return array(
    'already_in_destination',
    'already_in_message',
  );
}