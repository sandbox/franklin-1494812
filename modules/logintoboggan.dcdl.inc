<?php

function logintoboggan_dcdl_api() {
  return array('version' => 1);
}

function logintoboggan_config_variables() {
  return array(
    'logintoboggan_login_block_type',
    'logintoboggan_login_block_message',
    'logintoboggan_login_with_email',
    'logintoboggan_confirm_email_at_registration',
    'logintoboggan_pre_auth_role',
    'logintoboggan_purge_unvalidated_user_interval',
    'logintoboggan_redirect_on_register',
    'logintoboggan_redirect_on_confirm',
    'logintoboggan_login_successful_message',
    'logintoboggan_minimum_password_length',
    'logintoboggan_immediate_login_on_register',
    'logintoboggan_override_destination_parameter',
  );
}