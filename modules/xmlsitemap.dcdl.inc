<?php

function xmlsitemap_dcdl_api() {
  return array('version' => 1);
}

function xmlsitemap_config_variables() {
  return array(
    'xmlsitemap_minimum_lifetime',
    'xmlsitemap_xsl',
    'xmlsitemap_chunk_size',
    'xmlsitemap_batch_limit',
    'xmlsitemap_path',
    'xmlsitemap_base_url',
    'xmlsitemap_lastmod_format',
    'xmlsitemap_developer_mode',
  );
}

function xmlsitemap_config_tables() {
  return array(
    'xmlsitemap_sitemap',
  );
}