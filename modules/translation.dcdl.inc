<?php

function translation_dcdl_api() {
  return array('version' => 1);
}

function translation_config_variables() {
  $vars = array();
  
  foreach (node_get_types() as $type => $object) {
    $vars[] = 'language_content_type_' . $type;
  }
  
  return $vars;
}