<?php

function fbconnect_stream_publish_dcdl_api() {
  return array('version' => 1);
}

function fbconnect_stream_publish_config_variables() {
  $variables = array();
  
  foreach (array_keys(node_get_types('names')) as $type_name) {
    $variables[] = 'fbconnect_stream_publish_onoff_' . $type_name;
  }
  
  return $variables;
}