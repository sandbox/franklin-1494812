<?php

function aggregator_dcdl_api() {
  return array('version' => 1);
}

function aggregator_config_variables() {
  return array(
    'aggregator_allowed_html_tags',
    'aggregator_summary_items',
    'aggregator_clear',
    'aggregator_category_selector',
  );  
}

function aggregator_config_tables() {
  return array('aggregator_categories', 'aggregator_feeds', 'aggregator_category_feed');
}