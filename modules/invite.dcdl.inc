<?php

function invite_dcdl_api() {
  return array('version' => 1);
}

function invite_config_variables() {
  $vars = array(
    'invite_target_role_default',
    'invite_expiry',
    'invite_registration_path',
    'invite_subject',
    'invite_subject_editable',
    'invite_default_mail_template',
    'token_help',
    'invite_use_users_email',
    'invite_use_users_email_replyto',
    'invite_manual_from',
    'invite_manual_reply_to',
    'invite_page_title',
  );
  
  $roles = user_roles(FALSE, 'send invitations');
  foreach($roles as $rid => $role) {
    $vars[] = 'invite_target_role_' . $rid;
    $vars[] = 'invite_maxnum_' . $rid;
  }
  
  return $vars;
}