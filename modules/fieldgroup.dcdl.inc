<?php

function fieldgroup_dcdl_api() {
  return array('version' => 1);
}

function fieldgroup_config_tables() {
  return array(
    'content_group',
    'content_group_fields',
  );
}