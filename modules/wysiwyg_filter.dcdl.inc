<?php

include_once(drupal_get_path('module', 'wysiwyg_filter') . '/wysiwyg_filter.inc');

function wysiwyg_filter_dcdl_api() {
  return array('version' => 1);
}

function wysiwyg_filter_config_variables() {
  $variables = array();
  
  $formats = filter_formats();
  foreach (array_keys(filter_formats()) as $format) {
    $variables[] = 'wysiwyg_filter_valid_elements_parsed_' . $format;
    $variables[] = 'wysiwyg_filter_valid_elements_raw_' . $format;
    $variables[] = 'wysiwyg_filter_allow_comments_' . $format;
    $variables[] = 'wysiwyg_filter_nofollow_policy_' . $format;
    $variables[] = 'wysiwyg_filter_nofollow_domains_' . $format;
    $variables[] = 'wysiwyg_filter_styles_' . $format;
    
    foreach (array_keys(wysiwyg_filter_get_advanced_rules()) as $rule_key) {
      $variables[] = 'wysiwyg_filter_'. $rule_key .'_'. $format;
    }
  }
  
  return $variables;
}