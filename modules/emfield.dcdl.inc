<?php

function emfield_dcdl_api() {
  return array('version' => 1);
}

function emfield_config_variables() {
  $vars = array(
    'emfield_flvmediaplayer_url',
    'emfield_flv_profile',
    'emfield_imagerotator_url',
    'emfield_swfobject',
    'emfield_swfobject_location',
    'emfield_thumbnail_overlay',
  );
  
  return $vars;
}

function _emfield_module_provider_vars($module) {
  $providers = emfield_system_list($module);
  $vars = array();
  
  foreach($providers as $p) {
    $vars[] = 'emfield_' . $module . "_allow_" . $p->name;
  }
  return $vars;
}

function emaudio_dcdl_api() {
  return array('version' => 1);
}

function emaudio_config_variables() {
  $vars = _emfield_module_provider_vars('emaudio');
  
  $vars[] = 'emaudio_default_audio_width';
  $vars[] = 'emaudio_default_audio_height';
  $vars[] = 'emaudio_default_preview_width';
  $vars[] = 'emaudio_default_preview_height';
  $vars[] = 'emaudio_default_thumbnail_width';
  $vars[] = 'emaudio_default_thumbnail_height';
  $vars[] = 'emaudio_default_thumbnail_path';
  
  return $vars;
}

function emthumb_dcdl_api() {
  return array('version' => 1);
}

function emthumb_config_variables() {
  return _emfield_module_provider_vars('emvideo');
}

function emvideo_dcdl_api() {
  return array('version' => 1);
}

function emvideo_config_variables() {
  return _emfield_module_provider_vars('emvideo');
}