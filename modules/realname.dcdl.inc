<?php

function realname_dcdl_api() {
  return array('version' => 1);
}

function realname_config_variables() {
  return array(
    'realname_nodeapi',
    'realname_use_title',
    'realname_view',
    'realname_myacct',
    'realname_search_enable',
    'realname_user_disable',
    'realname_search_login',
    'realname_theme',
    'realname_max_username',
    'realname_notver',
    'realname_fields',
    'realname_homepage',
    'realname_nofollow',
    'realname_notver',
    'realname_pattern',
    'realname_profile_module',
    'realname_profile_type',
  );
}