<?php

function location_addanother_dcdl_api() {
  return array('version' => 1);
}

function location_addanother_config_variables() {
  $variables = array();
  
  foreach(array_keys(node_get_types()) as $node_type) {
    $variables[] = 'location_addanother_'. $node_type;    
  }
  
  return $variables;
}