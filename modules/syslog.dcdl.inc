<?php

function syslog_dcdl_api() {
  return array('version' => 1);
}

function syslog_config_variables() {
  return array(
    'syslog_identity',
    'syslog_facility',
  );
}