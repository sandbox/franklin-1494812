<?php

function context_dcdl_api() {
  return array('version' => 1);
}

function context_ui_dcdl_api() {
  return array('version' => 1);
}

function context_config_variables() {
  return array(
    'context_ui_show_empty_regions',
    'context_reaction_block_disable_core',
    'context_reaction_block_all_regions',
  );
}

/* TODO: This needs work.  The conditions field may include items that need to be UUID mapped. */
function context_config_tables() {
  return array(
    'context' => array(
      'no uuid map' => array('name'),
    ),
  );
}