<?php

function statistics_dcdl_api() {
  return array('version' => 1);
}

function statistics_config_variables() {
  return array(
    'statistics_enable_access_log',
    'statistics_flush_accesslog_timer',
    'statistics_count_content_views',
  );
}
