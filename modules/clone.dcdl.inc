<?php

function clone_dcdl_api() {
  return array('version' => 1);
}

function clone_config_variables() {
  $vars = array(
    'clone_method',
    'clone_omitted',
    'clone_nodes_without_confirm',
    'clone_menu_links',
  );
  $types = node_get_types('names');
  foreach ($types as $type => $name) {
    $vars[] = 'clone_reset_'. $type;
  }
  
  return $vars;
}

function clone_config_tables() {
  return array(
  );
}