<?php

function special_menu_items_dcdl_api() {
  return array('version' => 1);
}

function special_menu_items_config_variables() {
  return array(
    'oldtheme_menu_item_link',
    'special_menu_items_nolink_tag',
    'special_menu_items_seperator_tag',
    'special_menu_items_seperator_value',
  );
}