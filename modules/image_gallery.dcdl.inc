<?php

function image_gallery_dcdl_api() {
  return array('version' => 1);
}

function image_gallery_config_variables() {
  return array(
    'image_images_per_page',
    'image_gallery_nav_vocabulary',
    'image_gallery_node_info',
    'image_gallery_sort_order',
  );
}