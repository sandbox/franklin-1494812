<?php

function memcache_admin_dcdl_api() {
  return array('version' => 1);
}

function memcache_admin_config_variables() {
  return array(
    'show_memcache_statistics',
  );
}