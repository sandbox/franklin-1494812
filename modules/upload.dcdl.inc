<?php

function upload_dcdl_api() {
  return array('version' => 1);
}

function upload_config_variables() {
  $vars = array(
    'upload_extensions_default',
    'upload_uploadsize_default',
    'upload_usersize_default',
    'upload_max_resolution',
    'upload_list_default',
  );
  
  foreach (user_roles() as $rid => $role) {
    $vars[] = 'upload_extensions_' . $rid;
    $vars[] = 'upload_uploadsize_' . $rid;
    $vars[] = 'upload_usersize_' . $rid;
  }
  
  return $vars;
}