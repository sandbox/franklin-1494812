<?php

function premium_dcdl_api() {
  return array('version' => 1);
}

function premium_config_variables() {
  $vars = array(
    'premium_mode',
    'premium_time_count',
    'premium_time_unit',
    'premium_message',
    'premium_format',
    'premium_levels_info',
  );
  
  foreach (node_get_types('names') as $machine_name => $type_name) {
      $vars[] = 'premium_level_default_' . $machine_name;
  }

  return $vars;
}