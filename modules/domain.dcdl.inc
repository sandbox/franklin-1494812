<?php

function domain_dcdl_api() {
  return array('version' => 1);
}

function domain_config_variables() {
  return array(
    'domain_root',
    'domain_sitename',
    'domain_scheme',
    'domain_behavior',
    'domain_debug',
    'domain_force_admin',
    'domain_sort',
    'domain_list_size',
    'domain_select_format',
    'domain_www',
    'domain_access_rules',
    'domain_search',
    'domain_seo',
    'domain_default_source',
    'domain_grant_all',
    'domain_cron_rule',
    'domain_xmlrpc_rule',
    'domain_paths',
    'domain_form_elements'
  );
}

function domain_config_tables() {
  return array(
    'domain',
  );
}

/* No additional configuration */
function domain_content_dcdl_api() {
  return array('version' => 1);
}

