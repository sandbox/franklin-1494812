<?php

function domain_settings_dcdl_api() {
  return array('version' => 1);
}

function domain_settings_config_variables() {
  return array(
    'domain_settings_behavior',
    'domain_settings_forms',
    'domain_settings_form_visibility',
  );
}