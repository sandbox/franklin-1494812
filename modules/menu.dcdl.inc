<?php

function menu_dcdl_api() {
  return array('version' => 1);
}

function menu_config_variables() {
  return array(
    'menu_default_node_menu', 
    'menu_primary_links_source',
    'menu_secondary_links_source',
  );
}

function menu_config_tables() {
  return array(
    'menu_custom' => array(
      'no uuid map' => array('menu_name'),
    )
  );
}