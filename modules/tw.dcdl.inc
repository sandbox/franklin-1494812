<?php

function tw_dcdl_api() {
  return array('version' => 1);
}

function tw_config_variables() {
  return array(
  
  );
}

function tw_config_tables() {
  return array(
    'tw_tables',
    'tw_columns',
    'tw_relationships',
  );
}