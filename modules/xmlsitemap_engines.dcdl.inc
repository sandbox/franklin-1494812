<?php

function xmlsitemap_engines_dcdl_api() {
  return array('version' => 1);
}

function xmlsitemap_engines_config_variables() {
  return array(
    'xmlsitemap_engines_engines',
    'xmlsitemap_engines_minimum_lifetime',
    'xmlsitemap_engines_submit_updated',
    'xmlsitemap_engines_custom_urls',
  );
}