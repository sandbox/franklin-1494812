<?php

function system_config_dcdl_api() {
  return array('version' => 1);
}

function system_config_tables() {
  return array(
    'actions_aid',
  );
}

function system_config_variables() {
  return array(
    'admin_theme',
    'node_admin_theme',
    'theme_default',
    'site_name',
    'site_slogan',
    'site_mission',
    'site_footer',
    'anonymous',
    'site_frontpage',
    'site_403',
    'site_404',
    'error_level',
    'cache',
    'cache_lifetime',
    'page_compression',
    'block-cache',
    'file_downloads',
    'preprocess_css',
    'preprocess_js',
    'image_toolkit',
    'feed_default_items',
    'feed_item_length',
    'date_default_timezone',
    'configurable_timezones',
    'date_first_day',
    'site_offline',
    'site_offline_message',
    'clean_url',    
  );
}

function system_dcdl_dump(&$config) {
  $system_dump_options = array(
    'filter' => "type='theme'",
    'no uuid map' => array('filename'),
  );
  $config['tables']['system'] = dcdl_dump_table('system', $system_dump_options);  
  
  $actions_dump_options = array(
    'foreign keys' => array(
      'aid' => array(
        'table' => 'actions_aid',
        'columns' => array('aid' => 'aid'),
      ),
    ),
    'filter' => 'aid IN (SELECT aid FROM {actions_aid})',
  );
  $mapped_actions = dcdl_dump_table('actions', $actions_dump_options);
  
  $actions_dump_options = array(
    'filter' => 'aid NOT IN (SELECT aid FROM {actions_aid})',
    'no uuid map' => array('aid'),
  );
  $unmapped_actions = dcdl_dump_table('actions', $actions_dump_options);


  $config['tables']['actions'] = array_merge($mapped_actions, $unmapped_actions);
}