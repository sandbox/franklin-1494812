<?php

function parser_common_syndication_dcdl_api() {
  return array('version' => 1);
}

function parser_common_syndication_config_variables() {
  return array(
    'accept_invalid_cert',
  );
}

function parser_common_syndication_config_tables() {
  return array(
    'parser_common_syndication' => array('url', 'etag'),
  );
}