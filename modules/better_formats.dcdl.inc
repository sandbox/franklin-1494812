<?php

function better_formats_dcdl_api() {
  return array('version' => 1);
}

function better_formats_config_variables() {
  return array(
    'better_formats_per_node_type',
    'better_formats_fieldset_title',
    'better_formats_long_tips_link_text',
  );
}

function better_formats_config_tables() {
  return array(
    'better_formats_defaults',
  );
}