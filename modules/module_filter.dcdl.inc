<?php

function module_filter_dcdl_api() {
  return array('version' => 1);
}

function module_filter_config_variables() {
  return array(
    'module_filter_tabs',
    'module_filter_count_enabled',
    'module_filter_visual_aid',
    'module_filter_dynamic_save_position',
  );
}