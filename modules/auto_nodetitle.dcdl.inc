<?php
function auto_nodetitle_dcdl_api() {
  return array('version' => 1);
}

function auto_nodetitle_config_variables() {
  $vars = array();

  foreach (node_get_types('names') as $type => $type_name) {
    $vars[] = 'ant_' . $type;
    $vars[] = 'ant_pattern_' . $type;
    $vars[] = 'ant_php_' . $type;
  }

  return $vars;
}