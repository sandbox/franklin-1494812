<?php

function click_heatmap_dcdl_api() {
  return array('version' => 1);
}

function click_heatmap_config_variables() {
  return array(
    'click_heatmap_scope',
    'click_heatmap_admin_username',
    'click_heatmap_admin_password',
    'click_heatmap_change_login',
  );
}
