<?php

function backup_migrate_dcdl_api() {
  return array('version' => 1);
}

function backup_migrate_config_variables() {
	return array(
	  'backup_migrate_file_name'
	);
}

function backup_migrate_config_tables() {
  return array(
    'backup_migrate_profiles',
    'backup_migrate_destinations',
    'backup_migrate_schedules',
  );
}