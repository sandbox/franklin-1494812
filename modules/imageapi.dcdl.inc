<?php

function imageapi_dcdl_api() {
  return array('version' => 1);
}

function imageapi_config_variables() {
  return array(
    'imageapi_image_toolkit',
  );
}

function imageapi_gd_dcdl_api() {
  return array('version' => 1);
}

function imageapi_gd_config_variables() {
  return array(
    'imageapi_jpeg_quality',
    'imageapi_crop_background',
    'imageapi_interlaced',
  );
}

function imageapi_imagemagick_dcdl_api() {
  return array('version' => 1);
}

function imageapi_imagemagick_config_variables() {
  return array(
    'imageapi_imagemagick_quality',
    'imageapi_imagemagick_convert',
    'imageapi_imagemagick_debugging',
  );
}