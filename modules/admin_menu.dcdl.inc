<?php

function admin_menu_dcdl_api() {
  return array('version' => 1);
}

function admin_menu_config_variables() {
  return array(
    'admin_menu_devel_modules_enabled',
    'admin_menu_margin_top',
    'admin_menu_position_fixed',
    'admin_menu_rebuild_links',
    'admin_menu_tweak_modules',
    'admin_menu_tweak_tabs',
  );
}
