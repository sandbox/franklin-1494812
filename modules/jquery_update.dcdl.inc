<?php

function jquery_update_dcdl_api() {
  return array('version' => 1);
}

function jquery_update_config_variables() {
  return array(
    'jquery_update_replace',
    'jquery_update_compression_type',
  );
}