<?php

function countdown_dcdl_api() {
  return array('version' => 1);
}

function countdown_config_variables() {
  return array(
    'countdown_event_name',
    'countdown_accuracy',
    'countdown_timestamp',
  );
}