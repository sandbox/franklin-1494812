<?php

function nice_menus_dcdl_api() {
  return array('version' => 1);
}

function nice_menus_config_variables() {
  $vars = array(
    'nice_menus_number',
    'nice_menus_js',
    'nice_menus_sf_delay',
    'nice_menus_sf_speed',
  );
  
  $nmvars = array(
    'nice_menus_name',
    'nice_menus_menu',
    'nice_menus_depth',
    'nice_menus_type',
  );
  $blocks = nice_menus_block('list');
  foreach(array_keys($blocks) as $delta) {
    foreach($nmvars as $v) {
      $vars[] = $v . '_' . $delta;      
    }
  }
  
  return $vars;
}
