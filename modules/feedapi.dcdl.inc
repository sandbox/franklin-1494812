<?php

function feedapi_dcdl_api() {
  return array('version' => 1);
}

function feedapi_config_variables() {
  return array(
    'feedapi_allowed_html_tags',
    'feedapi_allow_html_all',
    'feedapi_cron_percentage',
  );
}

function feedapi_config_tables() {
  return array(
    'feedapi',
  );
}

function feedapi_dcdl_dump(&$config) {
  foreach ($config['tables']['feedapi'] as &$feed) {
    unset($feed['next_refresh_time']);
  }
}