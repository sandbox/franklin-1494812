<?php

function nodequeue_dcdl_api() {
  return array('version' => 1);
}

function nodequeue_config_variables() {
  return array(
    'nodequeue_use_tab',
    'nodequeue_tab_display_max',
    'nodequeue_tab_name',
    'nodequeue_autocomplete_limit',
    'nodequeue_view_per_queue',
  );
}

function nodequeue_config_tables() {
  return array(
    'nodequeue_queue',
    'nodequeue_roles',
    'nodequeue_types',
    'nodequeue_subqueue',
  );
}