<?php

function lightbox2_dcdl_api() {
  return array('version' => 1);
}

function lightbox2_config_variables() {
  return array(
    'lightbox2_plus',
    'lightbox2G2_filter',

    //  the general settings.
    'lightbox2_lite',
    'lightbox2_use_alt_layout',
    'lightbox2_force_show_nav',
    'lightbox2_loop_items',
    'lightbox2_image_count_str',
    'lightbox2_page_count_str',
    'lightbox2_video_count_str',
    'lightbox2_disable_resize',
    'lightbox2_disable_zoom',
    'lightbox2_enable_login',
    'lightbox2_enable_contact',
    'lightbox2_enable_video',
    'lightbox2_flv_player_path',
    'lightbox2_flv_player_flashvars',
    'lightbox2_page_init_action',
    'lightbox2_page_list',
    'lightbox2_disable_these_urls',
    'lightbox2_imagefield_group_node_id',
    'lightbox2_imagefield_use_node_title',
    'lightbox2_view_image_text',
    'lightbox2_image_ncck_group_node_id',
    'lightbox2_show_caption',
    // Advanced settings.
    'lightbox2_js_location',
    'lightbox2_keys_close',
    'lightbox2_keys_previous',
    'lightbox2_keys_next',
    'lightbox2_keys_zoom',
    'lightbox2_keys_play_pause',
    'lightbox2_top_position',
    'lightbox2_border_size',
    'lightbox2_box_color',
    'lightbox2_font_color',
    'lightbox2_overlay_opacity',
    'lightbox2_overlay_color',
    'lightbox2_disable_close_click',
    'lightbox2_resize_sequence',
    'lightbox2_resize_speed',
    'lightbox2_fadein_speed',
    'lightbox2_slidedown_speed',

    //  iframe settings.
    'lightbox2_default_frame_width',
    'lightbox2_default_frame_height',
    'lightbox2_frame_border',

    //  slideshow settings.
    'lightbox2_slideshow_interval',
    'lightbox2_slideshow_automatic_start',
    'lightbox2_slideshow_automatic_exit',
    'lightbox2_slideshow_show_play_pause',
    'lightbox2_slideshow_pause_on_next_click',
    'lightbox2_slideshow_pause_on_previous_click',
    'lightbox2_loop_slides',

    //  automatic image handling settings.
    'lightbox2_image_node',
    'lightbox2_display_image_size',
    'lightbox2_trigger_image_size',
    'lightbox2_disable_nested_galleries',
    'lightbox2_flickr',
    'lightbox2_gallery2_blocks',
    'lightbox2_inline',
    'lightbox2_image_assist_custom',
    'lightbox2_custom_class_handler',
    'lightbox2_custom_trigger_classes',
    'lightbox2_node_link_text',
    'lightbox2_download_link_text',
    'lightbox2_node_link_target',
    'lightbox2_show_caption',
    'lightbox2_image_group',
    'lightbox2_disable_nested_acidfree_galleries',
    'lightbox2_acidfree_video',
  );
}