<?php

function imagecache_dcdl_api() {
  return array('version' => 1);
}

function imagecache_config_tables() {
  return array(
    'imagecache_preset',
    'imagecache_action',
  );
}

function imagecache_ui_dcdl_api() {
  return array('version' => 1);
}

/* Imagecache UI saves no configuraiton data */