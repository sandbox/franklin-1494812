<?php

function coming_soon_dcdl_api() {
  return array('version' => 1);
}

function coming_soon_config_variables() {
  $vars = array(
    'coming_soon_admin_notice',
    'coming_soon_admin_watermark',
    'coming_soon_site_wide_page',
    'coming_soon_site_wide_excluded_paths',
  );
  
  $content_types = node_get_types('names');
  foreach($content_types as $machine_name => $human_name) {
    $vars[] = 'coming_soon_nodetype_' . $machine_name;
  }
  
  return $vars;
}