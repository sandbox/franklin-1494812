<?php

function workflow_dcdl_api() {
  return array('version' => 1);
}

function workflow_config_variables() {
  $vars = array(
    'workflow_states_per_page',
  );
  
  foreach (node_get_types() as $type => $name) {
    $vars[] = 'workflow_'. $type;
  }
  
  return $vars;
}

function workflow_config_tables() {
  return array(
    'workflows',
    'workflow_type_map',
    'workflow_transitions',
    'workflow_states',
  );
}