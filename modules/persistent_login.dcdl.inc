<?php

function persistent_login_dcdl_api() {
  return array('version' => 1);
}

function persistent_login_config_variables() {
  return array(
    'persistent_login_welcome',
    'persistent_login_maxlife',
    'persistent_login_maxlogins',
    'persistent_login_secure',
    'persistent_login_pages',
  );
}