<?php

function taxonomy_dcdl_api() {
  return array('version' => 1);
}

function taxonomy_config_variables() {
  return array(
    'taxonomy_terms_per_page_admin',
    'taxonomy_override_selector',
  );
}

function taxonomy_config_tables() {
  return array(
    'vocabulary', 
    'vocabulary_node_types',
  );
}

function taxonomy_dcdl_dump(&$config) {
  $th_fks = array(
    'tid' => array(
      'table' => 'term_data',
      'columns' => array('tid' => 'tid', 'parent' => 'tid'),
    ),
  );
  /* Don't dump tags, only normal taxonomy */
  $vocabs = array();
  $result = db_query('SELECT vid FROM vocabulary WHERE tags=0');
  while ($record = db_fetch_array($result)) {
    $vocabs[] = $record['vid'];
  };
  $vocab_list = implode(',', $vocabs);
  
  if (!empty($vocab_list)) {
    $where = "vid IN ($vocab_list)";
    $th_where = "tid IN (SELECT tid FROM {term_data} WHERE $where)";
  }
  $config['tables']['term_data'] = dcdl_dump_table('term_data', array('filter' => $where));
  $config['tables']['term_hierarchy'] = dcdl_dump_table('term_hierarchy', array('foreign keys' => $th_fks, 'filter' => $th_where));
}