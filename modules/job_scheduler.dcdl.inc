<?php

function job_scheduler_dcdl_api() {
  return array('version' => 1);
}

/* This module provides an API for other modules to schedule jobs.  
 * The only table is filled with state data, and should not be 
 * considered configuration. 
 */