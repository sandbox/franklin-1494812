<?php

function user_delete_dcdl_api() {
  return array('version' => 1);
}

function user_delete_config_variables() {
  return array(
    'user_delete_redirect',
    'user_delete_backup',
    'user_delete_backup_period',
    'user_delete_default_action',
  );
}